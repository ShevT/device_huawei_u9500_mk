#!/bin/sh

echo ""
echo "***********************"
echo "* Device tree patches *"
echo "***********************"

dir=`cd $(dirname $0) && pwd`
top=$dir/../../../..

#### Reverts ####
#
# MoKeeLauncher
cd $top/packages/apps/MoKeeLauncher/
# Revert: MoKeeLauncher: add YuBrowser to workspace
git revert --no-edit 1f627ff2bfda99a52fe7d3ec567008576b02b03b

cd $top

# Patches
for patch in `ls $dir/*.patch` ; do
    echo ""
    echo "==> patch file: ${patch##*/}"
    patch -p1 -N -i $patch -r - -d $top
done

find . -name "*.orig" -delete
